<?php

function random_char($string){
	$i = random_int(0,strlen($string)-1);
    return $string[$i];
}

function random_string($length, $char_set){
    $output = '';

    for($i = 0; $i < $length; $i++){
	$output .= random_char($char_set);
   
   }

  return $output;
}

function generate_password($length){

	//character set
	$lower = 'abcdefghijklmnopqrstuvwxyz';
	$upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$numbers = '0123456789';
	$symbols = '$*?!-#&~%';


	$use_lower = isset($_GET['lower']) ? $_GET['lower'] : '0';
	$use_upper = isset($_GET['upper']) ? $_GET['upper'] : '0';
	$use_numbers = isset($_GET['numbers']) ? $_GET['numbers'] : '0';
	$use_symbols = isset($_GET['symbols']) ? $_GET['symbols'] : '0';

	$chars = '';
	if($use_lower =='1') { $chars .= $lower; }
	if($use_upper =='1') { $chars .= $upper; }
	if($use_numbers =='1') { $chars .= $numbers; }
	if($use_symbols =='1') { $chars .= $symbols; }


	return random_string($length, $chars);

}

?>