# PASSWORD GENERATOR #

### What's this for ###

* Generate password for any length
* Copy the generated password in clipboard

### View ###

![Screenshot](assets/password_generator.png)
