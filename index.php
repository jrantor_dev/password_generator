<?php 
include 'src/password_generator.php';

if(isset($_GET['length'])){
	$password = generate_password($_GET['length']);
}

else{
	$password = '';
}



//using rangr function

// $lower = implode(range('a', 'z'));
// $upper = implode(range('A', 'Z'));
// $numbers = implode(range('0', '9'));
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>Password Generator</title>
	<link rel="stylesheet" type="text/css" href="assets/css/materialize.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
  <nav>
    <div class="nav-wrapper">
      <a href="#" class="brand-logo center">Password Generator</a>
    </div>
  </nav>

<div class="center">
	<?php if($password != "")  {?>
	<p>Generated Password: <em class="blue-grey lighten-1"><?php echo $password; ?></em>
     <?php echo "<button class='btn waves-effect waves-light pulse' onclick='copyFunc()'>
     Copy
     <i class='material-icons right'>bookmark</i>

   </button>"; ?>
    </p> 

<?php } ?>

 </div> 

 <div class="center">

<form action="" method="GET">

<div class="row">

	<label class="input-field col s2 offset-l5">
	<span>Length:</span>
	<input type="text" name="length" value="<?php if(isset($_GET['length'])) {echo $_GET['length'];} ?>" required/>

   </label>
</div>
   <div class="row">
	<label class="input-field">
		<input type="checkbox" name="lower" value="1" <?php  if(isset($_GET['lower']) && $_GET['lower'] == 1) { echo'checked'; } ?> > 
		<span>Lowercase </span>
	</label>
 </div> 

 <div class="row">
 	<label class="input-field">
		<input type="checkbox" name="upper" value="1" <?php if( isset($_GET['upper']) && $_GET['upper'] == 1) { echo 'checked'; } ?> > 
		<span>Uppercase </span>
	</label>	
</div>


<div class="row">
	<label class="input-field">
		<input type="checkbox" name="numbers" value="1" <?php if( isset($_GET['numbers']) && $_GET['numbers'] == 1) { echo 'checked'; } ?> > 
		<span>Numbers </span>
	</label>
</div>	


<div class="row">	
	<label class="input-field">
		<input type="checkbox" name="symbols" value="1" <?php if( isset($_GET['symbols']) && $_GET['symbols'] == 1) { echo 'checked'; } ?> > 
		<span>Symbols </span>
	</label>	
</div>
     
<div class="input-field col s6">
        <button class="btn waves-effect waves-light" type="submit">Submit
          <i class="material-icons right">send</i>
        </button>

</div>

</form>

</div>
<script>
	function copyFunc(){
    let textS = <?php echo json_encode($password); ?>

    navigator.clipboard.writeText(textS);
    alert("Text Copied: " + textS);
}

</script>

<script src="assets/js/materialize.js" type="text/javascript" charset="utf-8"></script>

</body>

</html>